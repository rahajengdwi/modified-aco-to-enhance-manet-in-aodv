# Modified ACO to Enhance Manet in AODV
# Modified Ant Colony Optimization to Enhance MANET Routing in Adhoc on Demand Distance Vector

<b>Nama :</b> Rahajeng Dwi P<br>
<b>NRP  :</b> 05111540000033<br><br>


<b>KONSEP :
- Menambah konsep Pheromone Count value dari Ant Colony Optimization (ACO) pada paket RREQ dan RREP.
- Pheromone Count value itu sendiri di dapatkan dari 4 faktor, yaitu dari kekuatan sinyal, kepadatan jalur, residu energi (energi yang tersisa), serta hop count.
- Dalam pemilihan rute, pilih yang memiliki nilai Pheromone_count terbanyak.